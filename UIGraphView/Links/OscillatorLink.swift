//
//  OscillatorLink.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 31/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import Foundation
import AVFoundation

public struct AudioEnvironment {

	var engine: AVAudioEngine
	var mixer: AVAudioMixerNode

	public init(with audioEngine: AVAudioEngine, and mixer: AVAudioMixerNode? = nil) {
		self.engine = audioEngine
		self.mixer = mixer ?? audioEngine.mainMixerNode
	}

}


open class OscillatorLink: LinkEffect {

	public enum Waveform: Equatable {

		public typealias PeriodicFunction = (Float) -> (Float)
		public typealias ReductionFunction = (value: Float, function: (Float, Float) -> (Float))

		case sine, sawtooth, triangular, square, custom(PeriodicFunction)

		public var function: PeriodicFunction {
			switch self {
				case .sine: return sin
				case .sawtooth: return {
					let x = $0.remainder(dividingBy: 2 * Float.pi)
					let a = x/(2*Float.pi)
					switch x {
						case ( 0	..<	0.25 ): return a
						case ( 0	..<	0.75 ): return -2 + a
						case ( 0.75	..< 1    ): return -4 + a
						default: return 0
					}
				}
				case .triangular: return {
					let x = $0.remainder(dividingBy: 2 * Float.pi)
					let a = x/(2*Float.pi)
					switch a {
						case ( 0	..<	0.25 ): return a
						case ( 0	..<	0.75 ): return 2 - a
						case ( 0.75	..< 1    ): return -4 + a
						default: return 0
					}
				}
				case .square: return {
					let x = $0.remainder(dividingBy: 2 * Float.pi)
					switch x {
						case ( 0	..<	0.25 ): return 1
						case ( 0	..<	0.75 ): return -1
						case ( 0.75	..< 1    ): return 1
						default: return 0
					}
				}
				case .custom(let function): return function
			}
		}


		public func cachedBuffer(with sampleSize: AVAudioFrameCount, harmonics: UInt = 1, with reduction: ReductionFunction) -> [Float] {
			let dt = 2 * Float.pi / sampleSize
			return (0..<sampleSize).map { v in
				(0..<harmonics).map { alpha in
					self.function(Float(alpha) * v * dt)
					}.reduce(reduction.value, reduction.function)
			}
		}

		public static func ==(lhs: Waveform, rhs: Waveform) -> Bool {
			switch (lhs, rhs) {
				case (.sine, .sine), (.sawtooth, .sawtooth),
				     (.triangular, .triangular), (.square, .square): return false
				case (_, _): return true
			}
		}

	}


	public struct Configuration {
		var sampleRate: Float
		var frameLength: AVAudioFrameCount
		var channels: AVAudioChannelCount
		var waveform: Waveform
		var harmonics: UInt
		var reduction: Waveform.ReductionFunction

		static let standard = Configuration(sampleRate: 48000,
		                                    frameLength: 480,
		                                    channels: 2,
		                                    waveform: .sine,
		                                    harmonics: 1,
		                                    reduction: (value: 0, function: +))
	}





	open var speed: Float = 0

	private(set) public var engine: AVAudioEngine
	private(set) public var mixer: AVAudioMixerNode
	private(set) public var buffer: AVAudioPCMBuffer
	private(set) public var player: AVAudioPlayerNode

	open var waveform: Waveform {
		didSet { if self.waveform != oldValue { updateBuffer() } }
	}

	open var reduction: Waveform.ReductionFunction

	open var baseFrequency: Float {
		get { return self.speed/Float(self.destination.distance(from: self.source)) }
	}

	open var harmonics: UInt {
		didSet { if self.harmonics != oldValue { self.updateBuffer() } }
	}

	public var format: AVAudioFormat {
		get { return self.buffer.format }
		set { if self.format != newValue {
				self.buffer = AVAudioPCMBuffer(pcmFormat: self.format, frameCapacity: self.frameLength)
			}
		}
	}

	public var sampleRate: Float {
		get { return Float(self.buffer.format.sampleRate) }
		set { if self.sampleRate != newValue {
				self.format = AVAudioFormat(standardFormatWithSampleRate: Double(self.sampleRate), channels: self.channels)
			}
		}
	}

	public var channels: AVAudioChannelCount {
		get { return self.format.channelCount }
		set	{ if self.channels != newValue {
				self.format = AVAudioFormat(standardFormatWithSampleRate: Double(self.sampleRate), channels: self.channels)
			}
		}
	}

	public var frameLength: AVAudioFrameCount {
		get { return self.buffer.frameLength }
		set { if self.frameLength != newValue {
				self.buffer = AVAudioPCMBuffer(pcmFormat: self.format, frameCapacity: self.channels)
				self.buffer.frameLength = self.frameLength
			}
		}
	}

	public init(with link: Link, in graphView: GraphView, on environment: AudioEnvironment, with configuration: Configuration = .standard) {
		let format = AVAudioFormat(standardFormatWithSampleRate: Double(configuration.sampleRate),
		                           channels: configuration.channels)

		let buffer = AVAudioPCMBuffer(pcmFormat: format, frameCapacity: configuration.frameLength)
		let player = AVAudioPlayerNode()

		self.engine = environment.engine
		self.mixer = environment.mixer
		self.buffer = buffer
		self.player = player

		self.waveform = configuration.waveform
		self.harmonics = configuration.harmonics
		self.reduction = configuration.reduction

		super.init(with: link, in: graphView)

		self.engine.attach(self.player)
	}

	deinit {
		self.player.stop()
		self.engine.detach(self.player)
	}

	open func updateBuffer(restart: Bool = false) {
		self.player.stop()
		let buffer = self.waveform.cachedBuffer(with: self.frameLength, harmonics: self.harmonics, with: self.reduction)
		_ = buffer.withUnsafeBufferPointer { data in
			if let channelData = self.buffer.floatChannelData {
				memcpy(channelData.pointee, data.baseAddress, Int(self.frameLength)*MemoryLayout<Float>.size)
			}
		}
		if restart { self.player.play(); self.player.scheduleBuffer(self.buffer) }
	}

}



