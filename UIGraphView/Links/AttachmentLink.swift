//
//  AttachmentLink.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 01/04/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit

/** Attachment link effect class

An attachment link is a physical connection between two nodes.

This class connects a graph link with a `UIAttachmentBehavior`, sets its basic properties and
adds it to the `UIDynamicAnimator` of its `graphView`.

*/
open class AttachmentLink: LinkEffect {

	/// `UIDynamics` behavior ruling the link existence.
	open var linkBehavior: UIAttachmentBehavior!

	/// Rest length of the attachment behavior.
	open var length: CGFloat {
		get { return self.linkBehavior.length }
		set { self.linkBehavior.length = newValue }
	}

	override public init(with link: Link, in graphView: GraphView) {
		super.init(with: link, in: graphView)
		self.attach()
	}

	/// Attach the two endpoints with the represented attachment behavior.
	private func attach() {
		if let behavior = self.linkBehavior { self.graphView.animator.removeBehavior(behavior) }
		self.linkBehavior = UIAttachmentBehavior(
			item: input!,
			attachedTo: output!
		)
		self.linkBehavior.damping = 0.1
		self.linkBehavior.frequency = 2
		let length = (input.radius + output.radius)
		self.linkBehavior.length = length/CGFloat(self.link.weight)
		self.linkBehavior.frictionTorque = 1
		self.graphView.animator.addBehavior(self.linkBehavior)
	}

}
