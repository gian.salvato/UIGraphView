//
//  Sampler.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 29/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import AVFoundation

/** Basic sampler class

A sampler is an object that maps some set of parameters (and, occasionally, a custom buffer) to
a specific waveform that can be played through an audio interface.

*/
open class Sampler {

	/// Singleton class holding default values
	open class Default {

		/// Default sample rate.
		static let sampleRate: Double = 48000

		/// Default number of channels.
		static let channels: AVAudioChannelCount = 1

		/// Default size of the internal buffer.
		static let frameLength: AVAudioFrameCount = 360

		/// Default output bus identifier.
		static let bus: AVAudioNodeBus = 0
	}

	/// Audio engine for this sampler.
	open var engine: AVAudioEngine

	/// Output node this sampler will play thorugh.
	open var output: AVAudioPlayerNode

	/// Mixer node this sampler will use for output mixing.
	open var mixer: AVAudioMixerNode

	/// Buffer containing the cached sample data.
	open var buffer: AVAudioPCMBuffer

	/// Sample rate for this sampler.
	open var sampleRate = Default.sampleRate

	/// Number of channels for this smapler.
	open var channels = Default.channels

	/// Size of the internal buffer for this sampler.
	open var frameLength = Default.frameLength

	/// Bus number this sampler to get the player's format from.
	open var bus = Default.bus

	/// Number of generated harmonics.
	open var harmonics: UInt = 1

	/// Audio format descriptor for this sampler.
	open var format: AVAudioFormat

	/// Proportionality consant to be used in sample generation.
	private var alpha: Float { return 2 * Float.pi/Float(self.sampleRate) }

	/// Started state of this sampler.
	open var started = false

	/// Initialize a new sampler.
	public init() {
		self.engine = AVAudioEngine()
		self.output = AVAudioPlayerNode()

		self.format = AVAudioFormat(standardFormatWithSampleRate: Default.sampleRate,
		                            channels: Default.channels)

		self.mixer = engine.mainMixerNode
		self.buffer = AVAudioPCMBuffer(pcmFormat: self.format,
		                               frameCapacity: Default.frameLength)
		self.buffer.frameLength = Default.frameLength

		self.engine.attach(self.output)
		self.engine.connect(self.output, to: self.mixer, format: self.format)

	}

	/// Start audio engine
	///
	/// - Throws: will rethrow any error thrown by the engine during its start operations.
	private func startEngine() throws {
		do { try self.engine.start() } catch let error {
			print("Error starting engine!")
			throw error
		}
	}

	/// Stop audio engine.
	private func stopEngine() {
		self.engine.stop()
	}

	/// Play the sample currently contained in the buffer.
	///
	/// - Parameters: 
	///   - block: the block to be executed at the end of the playing time (if any).
	open func play(block: @escaping () -> () = {}) {
		do { try self.startEngine() } catch let error {
			fatalError(error.localizedDescription)
		}
		self.output.play()
		self.scheduleBuffer(block: block)
		self.started = true
	}

	/// Schedule the internal buffer for playing immediately.
	///
	/// - Parameters:
	///   - block: the block to be executed at the end of the playing time. Currently, this
	///            will never be called.
	///
	open func scheduleBuffer(block: @escaping () -> () = {}) {
		self.output.scheduleBuffer(self.buffer, at: nil, options: .loops)
	}

	/// Stop the buffer playback.
	open func stop() {
		self.output.stop()
		self.stopEngine()
		self.started = false
	}

	/// Executes the given block after interrupting and before resuming the buffer playback.
	///
	/// - Parameter block: the block to be executed during the paused state.
	open func whileInterrupted(do block: @escaping () -> ()) {
		self.stopEngine()
		block()
		if self.started { self.play() }
	}

	/// Update the sample rate of the internal buffer
	///
	/// - Parameters:
	///	  - value: the new value for the sample rate.
	///   - prepare: the block to be executed before restarting the playback.
	/// - TODO: currently, this does not manage to start the playback again.
	open func updateSampleRate(with value: Float, prepare: @escaping () -> () = {}) {
		if value == Float(self.format.sampleRate) { return }
		self.sampleRate = Double(value)
		self.whileInterrupted {
			self.format = AVAudioFormat(standardFormatWithSampleRate: Double(value), channels: self.channels)
			prepare()
		}
	}

	/// Update the sample buffer size of the internal buffer
	///
	/// - Parameters:
	///	  - value: the new value for the buffer size.
	///   - prepare: the block to be executed before restarting the playback.
	/// - TODO: currently, this does not manage to start the playback again.
	open func updateSampleSize(with value: AVAudioFrameCount, prepare: @escaping () -> () = {}) {
		if self.frameLength == value { return }
		self.frameLength = value
		self.whileInterrupted {
			if value > self.buffer.frameCapacity {
				self.buffer = AVAudioPCMBuffer(pcmFormat: self.format, frameCapacity: value)
			}
			self.buffer.frameLength = value
			prepare()
		}
	}

	/// Feed raw values to the internal buffer.
	///
	/// - Parameter values: an array of values representing the waveform levels at the given time.
	public func feed(values: [Float]) {
		guard let channelData = self.buffer.floatChannelData else {
			print("Cannot open channel")
			return
		}

		/* The input array must have as many values as the sample buffer. */
		assert(UInt32(values.count) == self.frameLength, "Incorrect size for input array")

		/* we assume that no sample will ever have more than Int.max frames */
		_ = values.withUnsafeBufferPointer { data in
			memcpy(channelData.pointee, data.baseAddress, Int(self.frameLength)*MemoryLayout<Float>.size)
		}

	}

	/// Simple periodic (not language-enforced) 1D single-valued function.
	public typealias PeriodicFunction = (Float) -> Float

	/// Feed a list of reciprocal vectors to the sampler.
	///
	/// Each reciprocal vector will get an associated frequency (for the time being, we
	/// assume a linear relation between the norm of the wave vector and the frequency even
	/// if we ALL know that this is not the case).
	///
	/// - Parameters:
	///   - vectors: the list of reciprocal vectors that compose the spectrum for this sampler.
	///   - fx: the actual function to be used for the harmonic composition.
	open func feed(vectors: [CGReciprocalVector], with fx: @escaping PeriodicFunction = sin) {
		DispatchQueue.global(qos: .background).async {
			/* We assume a linear relation between reciprocal vectors and frequencies. */
			let frequencies = vectors.map { Float($0.norm) }
			DispatchQueue.main.async { self.feed(frequencies: frequencies, with: fx) }
		}

	}

	/// Feed a list of frequencies to the sampler.
	///
	/// - Parameters:
	///   - frequencies: the list of frequencies the sample will be generated from.
	///   - fx: the actual function to be used for the harmonic composition.
	open func feed(frequencies: [Float], with fx: @escaping PeriodicFunction = sin) {
		DispatchQueue.global(qos: .background).async {
			let a = 1/frequencies.total
			DispatchQueue.main.async { self.feed(spectrum: frequencies.map { (amplitude: a, value: $0) }, with: fx) }
		}
	}

	/// Feed a list of frequencies and amplitudes to this sampler.
	///
	/// - Parameters:
	///   - spectrum: an array of tuples in the form `(amplitude, value)` such that each
	///               frequency `value` will contribute to the sample with the given `amplitude`.
	///   - fx: the actual function to be used for the harmonic composition.
	open func feed(spectrum: [(amplitude: Float, value: Float)], with fx: @escaping PeriodicFunction = sin) {
		DispatchQueue.global(qos: .background).async {
			let startFrame = self.output.lastRenderTime?.sampleTime ?? 0

			let buffers = spectrum.map {
				return self.buffer(with: Float($0.value),
								  amplitude: $0.amplitude, phase: startFrame,
								  fx: fx)
			}

			let buffer = buffers.dropFirst().reduce(buffers[0]) { accumulator, element in
				(0..<accumulator.count).map { accumulator[$0] + element[$0] }
			}

			DispatchQueue.main.async { self.feed(values: buffer) }
		}
	}

	/// Generates a raw buffer for a single frequency.
	///
	/// - Parameters:
	///   - frequency: the frequency of the resulting sound.
	///   - amplitude: the amplitude of the fundamental frequency of the generated wave.
	///   - phase: the phase of the generated wave
	///   - fx: the actual function to be used for the harmonic composition.
	/// - Returns: an array containing raw buffer values that can be fed to the `feed(values:)`
	///            function.
	/// - TODO: the phase parameter does not actually get used... Fix this!
	open func buffer(with frequency: Float, amplitude: Float = 1, phase: AVAudioFramePosition = 0, fx: @escaping PeriodicFunction = sin) -> [Float] {
		/*let startFrame = phase % AVAudioFramePosition(self.frameLength)*/
		let anglePerFrame = self.alpha * frequency
		return (0..<self.frameLength).map { frame in
			let baseAngle = anglePerFrame * (/*startFrame + */AVAudioFramePosition(frame))
			let form = (1...self.harmonics).reduce(Float(1)) { result, harmonic in
				let angle = baseAngle * harmonic
				return result * amplitude * fx(angle)/Float(harmonic)
			}
			return form
		}
	}

}


