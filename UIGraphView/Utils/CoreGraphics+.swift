//
//  CoreGraphics.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 09/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import CoreGraphics

public func +<T: FloatingPoint, U: Integer>(lhs: T, rhs: U)	 -> T { return lhs + T(rhs.toIntMax()) }
public func -<T: FloatingPoint, U: Integer>(lhs: T, rhs: U)	 -> T { return lhs - T(rhs.toIntMax()) }
public func *<T: FloatingPoint, U: Integer>(lhs: T, rhs: U)	 -> T { return lhs * T(rhs.toIntMax()) }
public func /<T: FloatingPoint, U: Integer>(lhs: T, rhs: U)	 -> T { return lhs / T(rhs.toIntMax()) }

public func +<T: Integer, U: FloatingPoint>(lhs: T, rhs: U)	 -> U { return U(lhs.toIntMax()) + rhs }
public func -<T: Integer, U: FloatingPoint>(lhs: T, rhs: U)	 -> U { return U(lhs.toIntMax()) - rhs }
public func *<T: Integer, U: FloatingPoint>(lhs: T, rhs: U)	 -> U { return U(lhs.toIntMax()) * rhs }
public func /<T: Integer, U: FloatingPoint>(lhs: T, rhs: U)	 -> U { return U(lhs.toIntMax()) / rhs }


extension Sequence where Self.Iterator.Element == CGPoint {

	var distanceVectors: [CGVector] {
		return self.mapPairs { $1 - $0 }.map { $0.1 }
	}

	var distances: [CGFloat] {
		return self.distanceVectors.map { $0.norm }
	}

}


extension CGPoint {

	public static func +(lhs: CGPoint, rhs: CGVector)		-> CGPoint { return CGPoint(x: lhs.x + rhs.dx, y: lhs.y + rhs.dy) }
	public static func -(lhs: CGPoint, rhs: CGVector)		-> CGPoint { return CGPoint(x: lhs.x - rhs.dx, y: lhs.y - rhs.dy) }
	public static func -(lhs: CGPoint, rhs: CGPoint)		-> CGVector { return CGVector(dx: rhs.x - lhs.x, dy: rhs.y - lhs.y) }

	public init<I: Integer>(x: I, y: I) { self = CGPoint(x: CGFloat(x.toIntMax()), y: CGFloat(y.toIntMax())) }

	public static var random: CGPoint { return CGPoint(x: arc4random(), y: arc4random()) }

	public func distance(from point: CGPoint) -> CGFloat { return (point - self).norm }

	public var cgVector: CGVector { return CGVector(dx: self.x, dy: self.y) }

}

extension CGVector {

	public static func +(lhs: CGVector, rhs: CGVector)	-> CGVector { return CGVector(dx: lhs.dx + rhs.dx, dy: lhs.dy + rhs.dy) }
	public static func -(lhs: CGVector, rhs: CGVector)	-> CGVector { return CGVector(dx: lhs.dx - rhs.dx, dy: lhs.dy - rhs.dy) }
	public static func *(lhs: CGVector, rhs: CGVector)	-> CGFloat  { return CGFloat(lhs.dx*rhs.dx + lhs.dy*rhs.dy) }

	public static func *(lhs: CGFloat, rhs: CGVector)	-> CGVector { return CGVector(dx: lhs*rhs.dx, dy: lhs*rhs.dy) }
	public static func *(lhs: Int, rhs: CGVector)		-> CGVector { return CGFloat(lhs)*rhs }
	public static func *(lhs: Double, rhs: CGVector)	-> CGVector { return CGFloat(lhs)*rhs }

	public static func /(lhs: CGVector, rhs: CGFloat)	-> CGVector { return CGVector(dx: lhs.dx/rhs, dy: lhs.dy/rhs) }
	public static func /(lhs: CGVector, rhs: Int)		-> CGVector { return lhs/CGFloat(rhs) }
	public static func /(lhs: CGVector, rhs: Double)	-> CGVector { return lhs/CGFloat(rhs) }

	public static var random: CGVector { return CGPoint.random.cgVector }

	public var squaredNorm: CGFloat { return self * self }
	public var norm: CGFloat { return sqrt(self.squaredNorm) }

	public func cgPoint(from point: CGPoint) -> CGPoint { return CGPoint(x: point.x + self.dx, y: point.y + self.dy) }
	public var cgPoint: CGPoint { return self.cgPoint(from: CGPoint.zero) }

	public static func unit(with angle: CGFloat = 0) -> CGVector { return CGVector(dx: cos(angle), dy: sin(angle)) }

	public static func pointing(towards point: CGPoint) -> CGVector { return point.cgVector.normalized }
	public var normalized: CGVector { return self/self.norm }

	public var angle: CGFloat { return atan2(self.dy, self.dx) }

}

extension CGSize {

	public static func +(lhs: CGSize, rhs: CGSize)	-> CGSize { return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height) }
	public static func -(lhs: CGSize, rhs: CGSize)	-> CGSize { return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height) }

	public static func *(lhs: CGFloat, rhs: CGSize) -> CGSize { return CGSize(width: lhs*rhs.width, height: lhs*rhs.height) }
	public static func *(lhs: Int, rhs: CGSize)		-> CGSize { return CGFloat(lhs)*rhs }
	public static func *(lhs: Double, rhs: CGSize)	-> CGSize { return CGFloat(lhs)*rhs }

	public static func /(lhs: CGSize, rhs: CGFloat) -> CGSize { return CGSize(width: lhs.width/rhs, height: lhs.height/rhs) }
	public static func /(lhs: CGSize, rhs: Int)		-> CGSize { return lhs/CGFloat(rhs) }
	public static func /(lhs: CGSize, rhs: Double)	-> CGSize { return lhs/CGFloat(rhs) }


	public init(with vector: CGVector) { (self.width, self.height) = (vector.dx, vector.dy) }
	public init(squareOf side: CGFloat) { self.init(width: side, height: side) }

	public static var random: CGSize { return CGSize(with: CGVector.random) }

	public var cgVector: CGVector { return CGVector(dx: self.width, dy: self.height) }

	public func cgRect(centeredIn point: CGPoint) -> CGRect { return CGRect(origin: point - (self/2).cgVector, size: self) }
	public var cgRect: CGRect { return CGRect(origin: CGPoint.zero, size: self) }

}

extension CGRect {

	public init(centeredIn point: CGPoint, with size: CGSize) {
		self = CGRect(origin: point - (size/2).cgVector, size: size)
	}

	public init(from origin: CGPoint, to point: CGPoint) {
		self = CGRect(origin: origin, size: CGSize(with: point.cgVector))
	}

	public static var random: CGRect {
		return CGRect(origin: CGPoint.random, size: CGSize.random)
	}

	public var center: CGPoint {
		get { return self.origin + (self.size/2).cgVector }
		set { self.origin = newValue - (self.size/2).cgVector }
	}

	public var randomPoint: CGPoint {
		let scale = 1/CGFloat(Int32.max)
		return self.origin + CGVector(
			dx: CGFloat(arc4random())*scale*self.size.width,
			dy: CGFloat(arc4random())*scale*self.size.height
		)
	}

	public var randomSubrect: CGRect {
		return CGRect(from: self.randomPoint, to: self.randomPoint)
	}

	public var randomInset: CGRect {
		return CGRect(centeredIn: self.center, with: CGSize(with: self.randomPoint.cgVector))
	}

}



public struct CGReciprocalVector: Equatable {
	public var kx, ky: CGFloat

	public var k: CGFloat { return CGVector(dx: kx, dy: ky).norm }

	public init(kx: CGFloat, ky: CGFloat) {
		(self.kx, self.ky) = (kx, ky)
	}

	public init(from points: (CGPoint, CGPoint)) {
		self.init(with: points.1 - points.0)
	}

	public init(with vector: CGVector) {
		self.kx = CGFloat(2 * CGFloat.pi) / vector.dx
		self.ky = CGFloat(2 * CGFloat.pi) / vector.dy
	}

	public static func ==(lhs: CGReciprocalVector, rhs: CGReciprocalVector) -> Bool {
		return lhs.kx == rhs.kx && lhs.ky == rhs.ky
	}

	public static func *(lhs: Int, rhs: CGReciprocalVector) -> CGReciprocalVector {
		return CGReciprocalVector(kx: lhs*rhs.kx, ky: lhs*rhs.ky)
	}

	public static func *(lhs: CGFloat, rhs: CGReciprocalVector) -> CGReciprocalVector {
		return CGReciprocalVector(kx: lhs*rhs.kx, ky: lhs*rhs.ky)
	}

	private var cgVector: CGVector { return CGVector(dx: self.kx, dy: self.ky) }

	public var squaredNorm: CGFloat { return self.cgVector.squaredNorm }
	public var norm: CGFloat { return self.cgVector.norm }

	public var angle: CGFloat { return self.cgVector.angle }
	
}




