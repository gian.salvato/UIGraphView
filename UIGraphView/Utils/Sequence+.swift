//
//  Sequence.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import Foundation

extension Sequence {

	public var pairs: [(Self.Iterator.Element, Self.Iterator.Element)] {
		return self.mapPairs { ($0, $1) }.map { $0.1 }
	}

	public func forEachPair(run block: @escaping (Self.Iterator.Element, Self.Iterator.Element) -> Void) {
		_ = self.mapPairs(with: block)
	}

	public func mapPairs<T>(with block: @escaping (Self.Iterator.Element, Self.Iterator.Element) -> T) -> [(pair: (Self.Iterator.Element, Self.Iterator.Element), T)] {
		let elements = self.enumerated()
		return elements.flatMap { i, lhs in
			elements.dropFirst(i + 1).map { j, rhs in
					(pair: (lhs, rhs), block(lhs, rhs))
			}
		}
	}

}

extension Sequence where Self.Iterator.Element: FloatingPoint {

	public var total: Self.Iterator.Element {
		return self.reduce(Self.Iterator.Element(0), +)
	}

	public var scaled: Self {
		guard let maxValue = self.max() else { return self }
		return self.map { ($0/maxValue) } as! Self
	}

	public var normalized: Self {
		let total = self.total
		return self.map { ($0/total) } as! Self
	}

}
