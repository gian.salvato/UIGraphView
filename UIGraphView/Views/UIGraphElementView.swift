//
//  GraphElementView.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 09/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit

/** Base class for elements that want to join a `GraphView`.

An element view is any object representing a graph element that can be added to a `GraphView`
as a subview.

*/
open class GraphElementView<T: GraphElementType>: UIView {

	/// Type of the represented graph element.
	public typealias ElementType = T

	/// Main color with which the graph element will be represented.
	open var color: CGColor? {
		get { return self.layer.borderColor }
		set { self.layer.borderColor = newValue }
	}

	/// Position of the graph element in the `graphView`.
	open var position: CGPoint {
		get { return self.center }
		set { self.center = newValue }
	}

}

/** Base class for effects that want to be shown in a `GraphView`.

An element effect is any object representing a side effect on a graph and that can be added
to a `GraphView` as a link effect. The `GraphView` is responsible for drawing its effects.

*/
open class GraphElementEffect<T: GraphElementType> {

	/// Type of the represented graph element.
	public typealias ElementType = T

	/// Main color with which the graph effect will be represented.
	open var color: CGColor?


	/// Draw the represented effect in the given rect.
	///
	/// - Parameter rect: the frame rect in which the drawing should happen.
	open func draw(_ rect: CGRect) {}
	
}
