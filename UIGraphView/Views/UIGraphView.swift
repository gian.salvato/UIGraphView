//
//  GraphView.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

//: Playground - noun: a place where people can play

import UIKit


/** All classes that want to respond to graph changes should conform to this protocol.

The protocol is an aggregation of the `NodeView` and `LinkEffect` delegate protocols.

*/
public protocol GraphViewDelegate: NodeViewDelegate, LinkEffectDelegate { }


/** Basic graph view.

A graph view is a view that visually represents a graph with all of its nodes and links.

This class is responsible for showing and evaluating the current graph state as well as
providing a way for end users to interact with the graph objects.

*/
open class GraphView: UIView, UIDynamicAnimatorDelegate {

	/// The represented graph.
	open var graph: Graph!

	/// The graph view delegate.
	open var delegate: GraphViewDelegate? {
		didSet {
			self.nodeViews.forEach { $0.delegate = self.delegate }
			self.linkEffects.forEach { $0.delegate = self.delegate }
		}
	}

	/// The `UIDynamicsAnimator` which will handle all physics behaviors for the represented graph.
	open lazy var animator: UIDynamicAnimator = { return UIDynamicAnimator(referenceView: self) }()

	/// A set of views, each representing a graph node.
	open var nodeViews: Set<NodeView> = [] { didSet {
		self.subviews.forEach { $0.removeFromSuperview() }
		self.nodeViews.forEach { self.addSubview($0) }
		let behavior = UICollisionBehavior(items: Array(self.nodeViews))
		behavior.translatesReferenceBoundsIntoBoundary = true

		self.animator.addBehavior(behavior)
	} }

	/// A set of link effects, each representing a graph link.
	open lazy var linkEffects: Set<LinkEffect> = { return Set(self.nodeViews.flatMap {
		$0.node.links.map { LinkEffect(with: $0, in: self) }
	}) }()

	open override func layoutSubviews() { self.setNeedsDisplay() }

	/// Get the link effect associated with a particular link,
	///
	/// - Parameter link: the link whose effect has to be found.
	/// - Returns: the effect representing the given link.
	open func effect(for link: Link) -> LinkEffect? {
		return self.linkEffects.first(where: { $0.link.hashValue == link.hashValue } )
	}

	/// Get the view associated with a particular node.
	///
	/// - Parameter node: the node whose view has to be found.
	/// - Returns: the view representing the requested node.
	open func view(for node: Node) -> NodeView? {
		return self.nodeViews.first(where: { $0.node.hashValue == node.hashValue })
	}

	/// Get the first node view at the specified point.
	///
	/// - Parameter point: the point under which to search for nodes.
	/// - Returns: the first node at tha specified point, or `nil` if no node can be found.
	func nodeView(at point: CGPoint) -> NodeView? {
		return self.nodeViews.first(where: {
			let location = self.convert(point, to: $0)
			return $0.bounds.contains(location)
		})
	}

	open override func draw(_ rect: CGRect) {
		self.linkEffects.forEach { $0.draw(rect) }
	}

}







