//
//  LinkEffect.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit

extension Link {

	/// Get or instantiate a link effect that represents this link in a `graphView`.
	///
	/// - Parameter graphView: the container graph view.
	/// - Returns: a link effect provided by the `graphView` or a new link effect.
	open func effect(in graphView: GraphView) -> LinkEffect {
		return graphView.effect(for: self) ?? LinkEffect(with: self, in: graphView)
	}
	
}

/// All classes that want to respond to link changes should conform to this protocol
public protocol LinkEffectDelegate {

	/// Specify whether the given link effect should be graphically shown in the `graphView`.
	///
	///	Defaults to `true`
	///
	/// - Parameters:
	///   - effect: the link effect to be shown.
	///   - rect: the rect in which the effect should be drawn.
	/// - Returns: a `Bool` determining if the effect should be represented in the `graphView`.
	func linkEffect(_ effect: LinkEffect, shouldDrawIn rect: CGRect) -> Bool

	/// Specify the draw path for the given link effect.
	///
	/// Defauls to `effect.path`
	///
	/// - Parameters:
	///   - effect: the link effect the path pertains to.
	///   - rect: the rect the path should be drawn into.
	/// - Returns: a `UIBezierPath` representing the visual path for the given effect.
	func linkEffect(_ effect: LinkEffect, bezierPathIn rect: CGRect) -> UIBezierPath
}

extension LinkEffectDelegate {

	public static func defaultLinkEffect(_ effect: LinkEffect, shouldDrawIn: CGRect) -> Bool {
		return true
	}

	public static func defaultLinkEffect(_ effect: LinkEffect, bezierPathIn: CGRect) -> UIBezierPath {
		return UIBezierPath(cgPath: effect.path)
	}

	public func linkEffect(_ effect: LinkEffect, shouldDrawIn rect: CGRect) -> Bool {
		return Self.defaultLinkEffect(effect, shouldDrawIn: rect)
	}

	public func linkEffect(_ effect: LinkEffect, bezierPathIn rect: CGRect) -> UIBezierPath {
		return Self.defaultLinkEffect(effect, bezierPathIn: rect)
	}
	
}

/** Basic link effect class.

A link effect is any side-effect that a link could have aside from its existence in the graph.
Link view are, by default, shown in the `graphView` as straiht black lines connecting the views
corresponding to the input and output nodes of the given link.

Subclasses may provide behaviors such as physics attachments, length evaluation and anything
that can cross your mind.

*/
open class LinkEffect: GraphElementEffect<Link>, Hashable {

	/// The `GraphView` in which this link effect exists.
	open weak var graphView: GraphView!

	/// The link represented by this effect.
	open var link: Link!

	/// The delegate for this link effect.
	open var delegate: LinkEffectDelegate?


	/// The node view associated with the input of this effect's link.
	open var input:		NodeView! { return graphView.view(for: self.link.input) }

	/// The node view associated with the output of this effect's link.
	open var output:	NodeView! { return graphView.view(for: self.link.output) }

	/// The source point for this link (i.e. the position of the input node of this link)
	open var source:		CGPoint! { return self.input.position }

	/// The destination point for this link (i.e. the position of the output node of this link)
	open var destination:	CGPoint! { return self.output.position }

	open var hashValue: Int { return input.node.hashValue * output.node.hashValue }
	open static func ==(lhs: LinkEffect, rhs: LinkEffect) -> Bool { return lhs.hashValue == rhs.hashValue }

	/// Initialize a link effect representing a link in a `graphView`.
	///
	/// - Parameters:
	///   - link: the represented link.
	///   - graphView: the container `graphView`.
	public init(with link: Link, in graphView: GraphView) {
		self.link = link
		self.graphView = graphView
		super.init()
		
		self.color = UIColor.black.cgColor
	}


	/// Then shortest path (in euclidean space) between the input and the output node views.
	open var path: CGMutablePath {
		let path = CGMutablePath()
		path.move(to: self.source)
		path.addLine(to: self.destination)
		return path
	}

	override open func draw(_ rect: CGRect) {
		if self.delegate?.linkEffect(self, shouldDrawIn: rect) ?? true {
			UIColor(cgColor: self.color ?? UIColor.clear.cgColor).set()
			let path = (delegate?.linkEffect(self, bezierPathIn: rect) ?? UIBezierPath(cgPath: self.path))
			path.stroke()
		}
	}

}
