//
//  UINodeView.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit

extension Node {

	/// Default node frame rect
	static var defaultRect: CGRect {
		return CGRect(x: 0, y: 0, width: 30, height: 30)
	}

	/// Get or instantiate a node view that represents this node in a `graphView`.
	///
	/// - Parameter graphView: the container graph view.
	/// - Returns: a node view provided by the `graphView` or a new node view.
	open func view(in graphView: GraphView) -> NodeView {
		return graphView.view(for: self) ?? NodeView(self, in: graphView)
	}

}

/// All classes that want to respond to node changes should conform to this protocol
public protocol NodeViewDelegate {

	/// Called before the node view snaps to a point
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willSnapTo: the point the view will snap to.
	func nodeView(_: NodeView, willSnapTo: CGPoint)

	/// Called after the node view snapped to a point
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willSnapTo: the point the view did snap to.
	func nodeView(_: NodeView, didSnapTo: CGPoint)

	/// Called before the node view gets freed from a point
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willBeFreedFrom: the point the view will be freed from.
	func nodeView(_: NodeView, willBeFreedFrom: CGPoint)

	/// Called after the node view got freed from a point
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willBeFreedFrom: the point the view got freed from.
	func nodeView(_: NodeView, wasFreedFrom: CGPoint)

	/// Called after the node view got dragged to a point.
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - wasDraggedTo: the point the view was dragged to
	func nodeView(_: NodeView, wasDraggedTo: CGPoint)

	/// Called after the node view got tapped at a point.
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - wasDraggedTo: the point the view was tapped at.
	func nodeView(_: NodeView, wasTappedAt: CGPoint)

	/// Called after the node view got double-tapped at a point.
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - wasDraggedTo: the point the view was double-tapped at.
	func nodeView(_: NodeView, wasDoubleTappedAt: CGPoint)

	/// Called before the node view gets moved to a point.
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willMoveTo: the point the view will move at.
	func nodeView(_: NodeView, willMoveTo: CGPoint)

	/// Called after the node view got moved to a point.
	///
	/// - Parameters:
	///   - _: the given `nodeView`.
	///   - willMoveTo: the point the view did move at.
	func nodeView(_: NodeView, didMoveTo: CGPoint)
}

extension NodeViewDelegate {

	public func nodeView(_ nodeView: NodeView, willSnapTo point: CGPoint) {}
	public func nodeView(_ nodeView: NodeView, didSnapTo point: CGPoint) {}

	public func nodeView(_ nodeView: NodeView, willBeFreedFrom point: CGPoint) {}
	public func nodeView(_ nodeView: NodeView, wasFreedFrom point: CGPoint) {}

	public func nodeView(_ nodeView: NodeView, wasDraggedTo point: CGPoint) {}
	public func nodeView(_ nodeView: NodeView, wasTappedAt point: CGPoint) {}
	public func nodeView(_ nodeView: NodeView, wasDoubleTappedAt point: CGPoint) {}

	public func nodeView(_: NodeView, willMoveTo: CGPoint) {}
	public func nodeView(_: NodeView, didMoveTo: CGPoint) {}

}

/** Basic node view class

A node view is any side effect that a node could have aside from its existence in the graph.
Node views are, by default, white opaque views with a colored border.

Subclasses may provide behavior such as call dispatching, tracking, dependency resolution or 
anything that can cross your mind.

*/
open class NodeView: GraphElementView<Node> {

	/// The `GraphView` in which this node view exists.
	open weak var graphView: GraphView!

	/// The node represented by this view.
	open var node: Node!

	/// The delegate for this node view.
	open var delegate: NodeViewDelegate?

	private var snapBehavior: UISnapBehavior? {
		willSet {
			switch (self.snapBehavior, newValue) {
				case let (.none, .some(behavior)):
					self.graphView.animator.addBehavior(behavior)
				case let (.some(behavior), .none):
					self.graphView.animator.removeBehavior(behavior)
				default: return
			}
		}
	}

	/// The point this view will snap to.
	open var snapPoint: CGPoint? {
		willSet {
			switch (self.snapPoint, newValue, self.snapBehavior) {
				case let (.some(_), .some(point), .some(behavior)):
					self.delegate?.nodeView(self, willSnapTo: point)
					behavior.snapPoint = point
					self.delegate?.nodeView(self, didSnapTo: point)
				case let (.none, .some(point), .none):
					self.delegate?.nodeView(self, willSnapTo: point)
					self.snapBehavior = UISnapBehavior(item: self, snapTo: point)
					self.delegate?.nodeView(self, didSnapTo: point)
				case (let p, .none, .some(_)):
					if let point = p { self.delegate?.nodeView(self, willBeFreedFrom: point) }
					self.snapBehavior = nil
					if let point = p { self.delegate?.nodeView(self, wasFreedFrom: point) }
				default: return
			}
		}
	}


	/// The drag gesture recognizer for this node.
	open var dragGesture: UIPanGestureRecognizer?

	/// The tap gesture recognizer for this node.
	open var tapGesture: UITapGestureRecognizer?

	/// The double-tap gesture recognizer for this node.
	open var doubleTapGesture: UITapGestureRecognizer?


	/// This structure describes the set of options to specify for a node view.
	public struct Options: OptionSet {
		public let rawValue: UInt16
		public init(rawValue: UInt16) { self.rawValue = rawValue }

		/// Determines whether the view will snap to the release point when dragged.
		static let shouldSnapToDragPoint	= Options(rawValue: 1 << 0)

		/// Determines whether the view will respond to tap gestures.
		static let shouldRespondToTap		= Options(rawValue: 1 << 1)

		/// Determines whether the view will respond to double-tap gestures.
		static let shouldRespondToDoubleTap	= Options(rawValue: 1 << 2)

		/// Determines whether the view will snap to a point when tapped.
		static let shouldSnapWhenTapped		= Options(rawValue: 1 << 3).union(Options.shouldRespondToTap)

		/// Determines whether the view will get freed from a point when tapped.
		static let shouldFreeWhenTapped		= Options(rawValue: 1 << 4).union(Options.shouldRespondToTap)
	}

	/// The options for this node view.
	open var options = Options(rawValue: UInt16.max)

	/// The radius of the circular border of this node view.
	open var radius: CGFloat {
		get { return min(self.frame.size.width, self.frame.size.height) / CGFloat(2) }
		set {
			let d = CGFloat(2) * newValue
			self.bounds.size = CGSize(width: d, height: d)
			self.layer.cornerRadius = self.bounds.size.width / CGFloat(2)
		}
	}

	/// The center of this node view in its `graphView` container.
	override open var center: CGPoint {
		willSet { self.delegate?.nodeView(self, willMoveTo: newValue) }
		didSet {
			self.delegate?.nodeView(self, didMoveTo: self.center)
			self.graphView.setNeedsLayout()
		}
	}

	/// Initialize a node view representing a node in a `graphView`.
	///
	/// - Parameters:
	///   - node: the represented node.
	///   - graphView: the container `graphView`
	required public init(_ node: Node, in graphView: GraphView) {
		self.node = node
		self.graphView = graphView
		let frame = CGSize(squareOf: 30).cgRect
		super.init(frame: frame)
		self.color = UIColor.random().cgColor

		self.layer.borderWidth = 1

		self.layer.cornerRadius = self.bounds.size.width / CGFloat(2)
		self.layer.masksToBounds = true
		self.backgroundColor = UIColor.white
		self.graphView.nodeViews.insert(self)

		let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(drag))
		dragGesture.minimumNumberOfTouches = 1
		dragGesture.maximumNumberOfTouches = 1

		self.dragGesture = dragGesture
		self.addGestureRecognizer(self.dragGesture!)

		self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
		self.tapGesture!.numberOfTapsRequired = 1
		self.addGestureRecognizer(self.tapGesture!)

		self.doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
		self.tapGesture!.numberOfTapsRequired = 2
		self.addGestureRecognizer(self.doubleTapGesture!)

	}

	/// The default action this node view will perform when dragged.
	@objc open func drag() {
		if let point = self.dragGesture?.location(in: self.graphView) {
			if self.options.contains(.shouldSnapToDragPoint) { self.snapPoint = point }
			self.delegate?.nodeView(self, wasDraggedTo: point)
		}
	}

	/// The default action this node view will perform when tapped.
	@objc open func tap() {
		guard self.options.contains(.shouldRespondToTap),
			let point = self.tapGesture?.location(in: self.graphView) else { return }

		if let _ = self.snapBehavior {
			if self.options.contains(.shouldFreeWhenTapped) { self.snapPoint = nil }
		} else {
			if self.options.contains(.shouldSnapWhenTapped) { self.snapPoint = point }
		}
		self.delegate?.nodeView(self, wasTappedAt: point)
	}

	/// The default action this node view will perform when double-tapped.
	@objc open func doubleTap() {
		guard self.options.contains(.shouldRespondToDoubleTap),
			let point = self.doubleTapGesture?.location(in: self.graphView) else { return }
		self.delegate?.nodeView(self, wasDoubleTappedAt: point)
	}

	required public init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
	override open func encode(with aCoder: NSCoder) { super.encode(with: aCoder) }

}
