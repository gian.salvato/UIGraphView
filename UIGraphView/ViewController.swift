//
//  ViewController.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit
import AVFoundation

//MARK: Helper classes

open class Geometry2D {

	public static func polygon(with count: UInt, inCircleOf radius: CGFloat, centeredIn point: CGPoint = CGPoint.zero) -> [CGPoint] {
		if count == 1 { return [point] }

		let dt = 2 * CGFloat.pi / count
		return (0..<count).map {
			let t = $0 * dt
			let dr = radius * CGVector.unit(with: t)
			return point + dr
		}
	}

}

open class GraphFactory {

	public static func new(with nodeCount: Int, linkingRules: [(Int, Int)]) -> Graph {
		let graph = Graph()
		let nodes = (0..<nodeCount).map { Node(named: "\($0)", in: graph) }
		(0..<linkingRules.count).forEach {
			let (source, destination) = linkingRules[$0]

			guard source < nodes.count && destination < nodes.count else {
				print("Can't add link: \(source) -> \(destination)")
				return
			}
			Link(in: graph,
			     from: nodes[source], to: nodes[destination],
			     named: "\(source) -> \(destination)")

		}

		return graph
	}

	public static func ring(with nodeCount: Int) -> Graph {
		return GraphFactory.new(with: nodeCount,
		                        linkingRules: (0..<nodeCount).map { ($0, ($0 + 1) % nodeCount) })
	}

	public static func string(with nodeCount: Int) -> Graph {
		return GraphFactory.new(with: nodeCount,
		                        linkingRules: (0..<nodeCount-1).map { ($0, $0 + 1) })
	}

}


open class GraphViewController: UIViewController,
	UIDynamicAnimatorDelegate,
GraphViewDelegate {

	//MARK: Core objects

	open var graph: Graph!
	open var sampler: Sampler!

	//MARK: Outlets

	@IBOutlet weak open var graphView: GraphView!

	@IBOutlet weak open var sampleRatePicker: UIPickerView!
	@IBOutlet weak open var sampleSizePicker: UIPickerView!

	@IBOutlet weak open var modeControl: UISegmentedControl!
	@IBOutlet weak open var waveformControl: UISegmentedControl!

	//MARK: Build view programmatically

	open func configureController() {
		self.navigationItem.title = "Sample Graph"

		let view = self.view!
		view.backgroundColor = UIColor.white

		let topGuide = self.topLayoutGuide
		let bottomGuide = self.bottomLayoutGuide

		let graphView = GraphView()
		let sampleRatePicker = UIPickerView()
		let sampleSizePicker = UIPickerView()
		let modeControl = UISegmentedControl()
		let waveformControl = UISegmentedControl()

		graphView.backgroundColor = UIColor.white

		modeControl.insertSegment(withTitle: "chamber", at: 0, animated: false)
		modeControl.insertSegment(withTitle: "chain", at: 1, animated: false)

		waveformControl.insertSegment(withTitle: "sin", at: 0, animated: false)
		waveformControl.insertSegment(withTitle: "saw", at: 1, animated: false)
		waveformControl.insertSegment(withTitle: "tri", at: 2, animated: false)
		waveformControl.insertSegment(withTitle: "sq", at: 3, animated: false)

		modeControl.addTarget(self, action: #selector(self.updateMode(_:forEvent:)),for: UIControlEvents.allEvents)
		waveformControl.addTarget(self, action: #selector(self.updateWaveform(_:forEvent:)),for: UIControlEvents.allEvents)

		graphView.translatesAutoresizingMaskIntoConstraints = false
		sampleRatePicker.translatesAutoresizingMaskIntoConstraints = false
		sampleSizePicker.translatesAutoresizingMaskIntoConstraints = false
		modeControl.translatesAutoresizingMaskIntoConstraints = false
		waveformControl.translatesAutoresizingMaskIntoConstraints = false

		let constraints = [
			NSLayoutConstraint(item: graphView,			attribute: .left,		relatedBy: .equal, toItem: view,				attribute: .left,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: graphView,			attribute: .right,		relatedBy: .equal, toItem: view,				attribute: .right,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: graphView,			attribute: .top,		relatedBy: .equal, toItem: topGuide,			attribute: .bottom,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: graphView,			attribute: .bottom,		relatedBy: .equal, toItem: sampleRatePicker,	attribute: .top,			multiplier: 1, constant: 16),

			NSLayoutConstraint(item: sampleRatePicker,	attribute: .left,		relatedBy: .equal, toItem: view,				attribute: .left,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: sampleRatePicker,	attribute: .right,		relatedBy: .equal, toItem: sampleSizePicker,	attribute: .left,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: sampleRatePicker,	attribute: .bottom,		relatedBy: .equal, toItem: modeControl,			attribute: .top,			multiplier: 1, constant: -8),

			NSLayoutConstraint(item: sampleSizePicker,	attribute: .right,		relatedBy: .equal, toItem: view,				attribute: .right,			multiplier: 1, constant: 0),

			NSLayoutConstraint(item: modeControl,		attribute: .left,		relatedBy: .equal, toItem: view,				attribute: .leftMargin,		multiplier: 1, constant: 0),
			NSLayoutConstraint(item: modeControl,		attribute: .right,		relatedBy: .equal, toItem: waveformControl,		attribute: .left,			multiplier: 1, constant: -8),
			NSLayoutConstraint(item: modeControl,		attribute: .bottom,		relatedBy: .equal, toItem: bottomGuide,			attribute: .top,			multiplier: 1, constant: -16),

			NSLayoutConstraint(item: waveformControl,	attribute: .right,		relatedBy: .equal, toItem: view,				attribute: .rightMargin,	multiplier: 1, constant: 0),

			NSLayoutConstraint(item: sampleRatePicker,	attribute: .width,		relatedBy: .equal, toItem: sampleSizePicker,	attribute: .width,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: modeControl,		attribute: .width,		relatedBy: .equal, toItem: view,				attribute: .width,			multiplier: 0.4, constant: 0),

			NSLayoutConstraint(item: sampleRatePicker,	attribute: .height,		relatedBy: .equal, toItem: sampleSizePicker,	attribute: .height,			multiplier: 1, constant: 0),
			NSLayoutConstraint(item: modeControl,		attribute: .height,		relatedBy: .equal, toItem: waveformControl,		attribute: .height,			multiplier: 1, constant: 0),

			NSLayoutConstraint(item: sampleRatePicker,	attribute: .centerY,	relatedBy: .equal, toItem: sampleSizePicker,	attribute: .centerY,		multiplier: 1, constant: 0),
			NSLayoutConstraint(item: modeControl,		attribute: .centerY,	relatedBy: .equal, toItem: waveformControl,		attribute: .centerY,		multiplier: 1, constant: 0),

			NSLayoutConstraint(item: sampleRatePicker,	attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 48)
		]

		view.addSubview(graphView)
		view.addSubview(sampleRatePicker)
		view.addSubview(sampleSizePicker)
		view.addSubview(modeControl)
		view.addSubview(waveformControl)

		view.addConstraints(constraints)

		self.graphView = graphView
		self.sampleRatePicker = sampleRatePicker
		self.sampleSizePicker = sampleSizePicker
		self.modeControl = modeControl
		self.waveformControl = waveformControl

		self.sampleRatePicker.selectRow(sampleRates.count/2, inComponent: 0, animated: false)
		self.sampleSizePicker.selectRow(sampleSizes.count/2, inComponent: 0, animated: false)
		self.modeControl.selectedSegmentIndex = 0
		self.waveformControl.selectedSegmentIndex = 0

	}

	//MARK: Manage GraphView

	open var nodeCount = 5
	open var nodeViews: Set<NodeView> {
		get { return self.graphView.nodeViews }
		set { self.graphView.nodeViews = newValue }
	}

	open var nodeRadius: CGFloat = 20

	open var linkEffects: Set<LinkEffect> {
		get { return self.graphView.linkEffects }
		set { self.graphView.linkEffects = newValue }
	}

	open var circleRadius: CGFloat = 100

	//MARK: Manage sound medium properties

	open var soundSpeed: Float = 360
	private var alpha: Float { return 440 * self.soundSpeed }


	public typealias Waveform = OscillatorLink.Waveform

	open var waveform: Waveform = .sine
	open var harmonics: UInt {
		get { return self.sampler.harmonics }
		set { self.sampler.harmonics = newValue }
	}

	public enum Mode {
		case chamber, chain
	}

	open var mode: Mode = .chamber

	open let sampleRates: [Float]				= [ 16, 32, 44.1, 48, 96 ]
	open let sampleSizes: [AVAudioFrameCount]	= [ 128, 192, 256, 384, 512, 768, 1024 ]


	open var navigationBarButtons: [UIBarButtonItem] {
		if self.sampler != nil && self.sampler.output.isPlaying {
			return [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop,
			                        target: self,
			                        action: #selector(self.stop))]
		} else {
			return [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.play,
			                        target: self,
			                        action: #selector(self.play)),
			        UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.organize,
			                        target: self,
			                        action: #selector(self.printSample))]
		}
	}

	//MARK: Manage view presentation

	override open func viewDidLoad() {
		super.viewDidLoad()
	}

	override open func viewWillAppear(_ animated: Bool) {
		self.navigationItem.rightBarButtonItems = self.navigationBarButtons
		self.view.layoutSubviews()

		if self.graphView == nil { self.configureController() }

		self.sampleRatePicker.selectRow(sampleRates.count/2, inComponent: 0, animated: animated)
		self.sampleSizePicker.selectRow(sampleSizes.count/2, inComponent: 0, animated: animated)

		self.sampleRatePicker.delegate = self
		self.sampleRatePicker.dataSource = self
		self.sampleSizePicker.delegate = self
		self.sampleSizePicker.dataSource = self

		self.sampler = Sampler()

		self.redrawGraph(with: self.nodeCount)
	}

	open func redrawGraph(with nodeCount: Int) {
		self.nodeCount = nodeCount

		self.graph = GraphFactory.ring(with: self.nodeCount)

		self.graphView.clipsToBounds = true

		self.nodeViews = Set(graph.nodes.map { $0.view(in: self.graphView) })
		self.graphView.linkEffects = Set(self.graph.links.map {
			AttachmentLink(with: $0, in: self.graphView)
		})

		let positions = Geometry2D.polygon(with: UInt(self.graph.nodes.count),
		                                   inCircleOf: self.circleRadius,
		                                   centeredIn: self.view.center - CGVector(dx: 0, dy: 50))

		let points = zip(positions, (0..<positions.count).map {
			(positions[($0 + 1)%positions.count] - positions[$0]).norm
		})

		zip(self.nodeViews.sorted {$0.node.hashValue < $1.node.hashValue}, points).forEach { view, point in

			view.radius = self.nodeRadius

			view.center = point.0
			view.snapPoint = view.center

			view.node.links.forEach { link in
				(link.effect(in: self.graphView) as? AttachmentLink)?.length = point.1
			}
			self.graphView.animator.updateItem(usingCurrentState: view)

		}

		self.graphView.delegate = self

		self.updateSample()

	}


	override open func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	override open func viewWillDisappear(_ animated: Bool) {
		self.sampler.stop()
	}

	//MARK: Manage sampler

	@objc open func play() {
		sampler.play() { self.updateSample() }
		self.navigationItem.rightBarButtonItems = self.navigationBarButtons
	}

	@objc open func stop() {
		sampler.stop()
		self.navigationItem.rightBarButtonItems = self.navigationBarButtons
	}

	@objc open func printSample() {
		guard let channelData = sampler.buffer.floatChannelData?.pointee else {
			print("Cannot get pointer to channel data.")
			return
		}
		print("{"); (0..<self.sampler.frameLength).forEach {
			print("{\($0),\(channelData.advanced(by: Int($0)).pointee)}\($0 != sampler.frameLength-1 ? "," : "")")
		}; print("}")

		let alert = UIAlertController(title: "Printed", message: "The sample raw data has been printed to stdout",
		                              preferredStyle: .alert)
		let okButton = UIAlertAction(title: "Ok", style: .default) { _ in
			alert.dismiss(animated: true)
		}

		alert.addAction(okButton)
		self.present(alert, animated: true)
	}

	open func updateSample() {
		var distances: [CGFloat]
		switch self.mode {
		case .chamber:
			distances = self.nodeViews.map { $0.center }.distanceVectors.map { $0.norm }
		case .chain:
			let views = Array(self.nodeViews.enumerated())
			distances = (0..<self.nodeViews.count).map {
				(views[$0].element.center - views[($0 + 1) % self.nodeViews.count].element.center).norm
			}
		}
		let reciprocalFigures = distances.map { self.alpha / Float($0) }
		let maxAmplitude = Float(distances.reduce(0, +))
		let spectrum = zip(distances, reciprocalFigures).map { (amplitude: Float($0)/maxAmplitude, value: $1) }

		self.sampler.feed(spectrum: spectrum, with: self.waveform.function)
	}


	//MARK: Manage graph updates

	open var ticksSinceLastUpdate: UInt64 = 0

	open func nodeView(_ nodeView: NodeView, didMoveTo point: CGPoint) {
		if self.ticksSinceLastUpdate % 5 == 0 {
			self.updateSample()
		}
		self.ticksSinceLastUpdate += 1
	}

}

//MARK: Manage pickers

extension GraphViewController: UIPickerViewDataSource {

	open func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		switch true {
		case pickerView === self.sampleRatePicker:
			return self.sampleRates.count
		case pickerView === self.sampleSizePicker:
			return self.sampleSizes.count
		default: return 0
		}

	}

	open func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

}

extension GraphViewController: UIPickerViewDelegate {

	open func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		switch true {
		case pickerView === self.sampleRatePicker:
			return row < sampleRates.count ? "\(sampleRates[row]) KHz" : nil
		case pickerView === self.sampleSizePicker:
			return row < sampleSizes.count ? "\(sampleSizes[row]) samples" : nil
		default: return nil
		}
	}


	open func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		switch true {
		case pickerView === self.sampleRatePicker:
			if let value = self.valueFrom(picker: pickerView)?.float {
				self.sampler.updateSampleRate(with: 1000 * value, prepare: self.updateSample)
			}
		case pickerView === self.sampleSizePicker:
			if let value = self.valueFrom(picker: pickerView)?.int {
				self.sampler.updateSampleSize(with: value, prepare: self.updateSample)
			}
		default: return
		}
	}

	open func valueFrom(picker: UIPickerView) -> (float: Float?, int: AVAudioFrameCount?)? {
		let row = picker.selectedRow(inComponent: 0)
		guard let valueString = self.pickerView(picker, titleForRow: row, forComponent: 0) else {
			return nil
		}
		guard let numberString = valueString.components(separatedBy: " ").first else {
			return nil
		}

		return (float: Float(numberString), int: AVAudioFrameCount(numberString))
	}

}

//MARK: Manage segmented controls

extension GraphViewController {

	@IBAction open func updateWaveform(_ sender: UISegmentedControl, forEvent event: UIEvent) {
		switch sender.selectedSegmentIndex {
		case 0: self.waveform = .sine
		case 1: self.waveform = .sawtooth
		case 2: self.waveform = .triangular
		case 3: self.waveform = .square
		default: return
		}
		self.updateSample()
	}

	@IBAction open func updateMode(_ sender: UISegmentedControl, forEvent event: UIEvent) {
		switch sender.selectedSegmentIndex {
		case 0: self.mode = .chamber
		case 1: self.mode = .chain
		default: return
		}
		self.updateSample()
	}
	
}





