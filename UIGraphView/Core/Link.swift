//
//  Link.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//


/// Protocol to which every graph link wannabe should conform.
public protocol LinkType: GraphElementType {

	/// Graph in which the link exists
	var graph: Graph! { get }

	/// Input node for this link
	var input: Node! { get set }

	/// Output node for this link
	var output: Node! { get set }

	/// Weight of this link
	var weight: Double { get set }
}


/** Basic link class

A link is an association between two nodes, supplemented by a given `weight`.

This class is responsible for holding the link endpoints, identifier and name, as well as an abstract `weight` 
on top of which additional logic can be constructed.

*/
open class Link: LinkType {

	open weak var graph: Graph!

	open weak var input: Node!
	open weak var output: Node!

	open var weight = 1.0

	/// Name of this link
	open var name = ""

	open var hashValue: Int { return input.hashValue * output.hashValue }

	/// Unique identifier for this link. The lazy property only gets set on first access.
	open lazy var id: Int = self.graph.uniqueLinkId

	open static func ==(lhs: Link, rhs: Link) -> Bool {
		return lhs.hashValue == rhs.hashValue
	}

	/// Initialize a link between two nodes in a graph.
	///
	/// - Parameters:
	///   - graph: the graph the link belongs to.
	///   - input: the input node.
	///   - output: the output node.
	///   - name: a name for this link.
	@discardableResult
	public init(in graph: Graph, from input: Node, to output: Node, named name: String? = nil) {
		self.graph = graph
		self.input = input
		self.output = output

		self.name = name ?? "\(self.input.name) -> \(self.output.name)"

		self.input.links.insert(self)
		self.output.links.insert(self)
	}
	
}
