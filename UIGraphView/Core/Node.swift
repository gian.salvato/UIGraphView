//
//  Node.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//


/// Protocol to which every graph node wannabe should conform.
public protocol NodeType: GraphElementType {

	/// Graph in which the node exists
	var graph: Graph! { get }

	/// Links that are attached to this node
	var links: Set<Link> { get set }
}


/** Basic node class.

A node is an abstract element of a graph, having no intrinsic properties except for its links.

This class is responsible for holding the node identifier and name as well as the set of links
that can provide additional behaviors to it.

*/
open class Node: NodeType {

	open weak var graph: Graph!

	open var links: Set<Link> = []

	/// Name of this node.
	open var name = ""

	/// Unique identifier for this node. The lazy property only gets set on first access.
	open lazy var id: Int = self.graph.uniqueNodeId

	open var hashValue: Int { return id }

	open static func ==(lhs: Node, rhs: Node) -> Bool {
		return lhs.hashValue == rhs.hashValue
	}

	/// Initialize a node in the given graph.
	///
	/// - Parameters:
	///   - name: a name for this node.
	///   - graph: the graph this node belongs to.
	@discardableResult
	public init(named name: String, in graph: Graph) {
		self.name = name
		self.graph = graph
		self.graph.nodes.insert(self)
	}
	
}
