//
//  Graph.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//


/// Protocol to which every graph element wannabe should conform.
public protocol GraphElementType: Hashable {

	/// Unique identifier for this element.
	var id: Int { get }
}


/** Basic graph class.

A graph is a collection of nodes connected by links.

This class is responsible for holding sets of nodes along with references to their
associated links.

**/
open class Graph {


	/// Global node id counter
	private var __uniqueNodeId = 0

	/// Global link id counter
	private var __uniqueLinkId = 0


	/// Provides an unique node id which gets incremented on read.
	private(set) public var uniqueNodeId: Int {
		get { defer { __uniqueNodeId += 1 }; return __uniqueNodeId }
		set { self.__uniqueNodeId = newValue }
	}


	/// Provides an unique link id which gets incremented on read.
	private(set) public var uniqueLinkId: Int {
		get { defer { __uniqueLinkId += 1 }; return __uniqueLinkId }
		set { self.__uniqueLinkId = newValue }
	}


	/** Graph node set. */
	public var nodes: Set<Node> = []


	/** Graph links set.
	
	This computed property will simply coalesce a set of all links pertaining to this graph
	by analyzing the set of nodes.

	*/
	public var links: Set<Link> { return Set(nodes.flatMap{ $0.links }) }


	/// Search the graph for the node with the specified id.
	///
	/// - Parameter id: the numeric identifier of the requested node
	/// - Returns: the requested node, or `nil` if no such node exists in the graph.
	public func node(with id: Int) -> Node? { return self.nodes.first(where: { $0.id == id }) }

	/// Search the graph for the link with the specified id.
	///
	/// - Parameter id: the numeric identifier of the requested link
	/// - Returns: the requested link, or `nil` if no such link exists in the graph.
	public func link(with id: Int) -> Link? { return self.links.first(where: { $0.id == id }) }


	/// Search for all nodes having a given name.
	///
	/// - Parameter name: the name of the requested nodes.
	/// - Returns: an `Array` of all nodes having the specified name.
	public func nodes(named name: String) -> [Node] { return self.nodes.filter { $0.name == name } }


	/// Search for all links having a given name.
	///
	/// - Parameter name: the name of the requested links.
	/// - Returns: an `Array` of all links having the specified name.
	public func links(named name: String) -> [Link] { return self.links.filter { $0.name == name } }

}
