# GraphView

This is a very simple implementation of a graph and a view to display it using UI Dynamics. It has been extended with an audio sampler in order to show an example of its capabilities.

_At the present time, it is pretty useless._

The code is structured as follows:

- `Core/*`: tiny core library, including `Graph`, `Node` and `Link` classes;
- `Views/*`: a `*View` class for each of the core classes;
- `Utils/*`: a set of handy extensions to some UIKit classes;
- `ViewController`: a simple view controller to display an example graph.
- 'Sampler': a simple sampler class that stores and plays an audio buffer that can be changed in "real time".

## Plans for the future:

The first-next features I'm planning to introduce are

- (half-done) a delegation-based mechanism for complex user interaction;
- specialized Node subclasses carrying special behaviors.

Other minor changes not worth listing right now.
